﻿using UnityEngine;
using System.Collections;

public class Lvlup : MonoBehaviour {

    float maxHealth;
    float nivel;
    int currentXP = 0;
    int nextLevelUp = 100;


    // Use this for initialization
    void Start()
    {
        nivel = 1;
    }

    void gainExperience(int amount)
    {
        currentXP += amount;
        if (currentXP >= nextLevelUp)
        {
            levelUp();
        }
    }


    void levelUp()
    {
        nextLevelUp += 100;
        maxHealth += 10;
    }
}